from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='A recommendation engine that matches job positions with possible applicants',
    author='Synx 2020',
    license='',
)
